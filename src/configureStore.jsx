import { configureStore } from "@reduxjs/toolkit";
import authSlice from "./Slices/authSlice";
import signUpSlice from "./Slices/signUpSlice";


const store = configureStore({
    reducer: {
        authSlice,
        signUpSlice,
    }
})

export default store