import React, { Component } from "react";
import Slider from "react-slick";

const img = process.env.PUBLIC_URL + "../../../hinhanh/bg-hero-1-1792-x1.png";

export default class SimpleSlider extends Component {
  render() {
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
    };
    return (
      // horization vertical
      <div data-slick='{"slidesToShow": 5, "slidesToScroll": 5}'>
        <div className="">
          <img className="w-full h-full " src={img} alt="" />
        </div>
        <div>{/* <img className="w-full h-full " src={img} alt="" /> */}</div>
        <div>
          <h3>3</h3>
        </div>
        <div>
          <h3>4</h3>
        </div>
        <div>
          <h3>5</h3>
        </div>
      </div>
    );
  }
}
