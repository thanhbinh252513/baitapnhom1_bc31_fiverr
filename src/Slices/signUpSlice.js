import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { message } from 'antd';
import { authAPI } from '../Service/authAPI';
import localService from '../Service/LocalService';

const initialState = {
    user: null,
    isLoading: false,
    error: null,
}

export const signUpAction = createAsyncThunk(
    'signUpAuth/SignUp',
    async (values) => {
        try {
            const data = await authAPI.signUpAPI(values)
            message.success("Sign Up Success")
            localService.user.set(data)

            window.location.href = "/"

            return data
        }
        catch (error) {
            message.error("Sign Up Failed")
            throw error
        }
    }
)

const signUpSlice = createSlice({
    name: 'signUpAuth',
    initialState,
    reducers: {
        resetAuthSignUp: (state) => {
            state.user = null;
            state.error = null;
        }
    },
    extraReducers: (builder) => {
        builder.addCase(signUpAction.pending, state => {
            state.isLoading = true
        });
        builder.addCase(signUpAction.fulfilled, (state, { payload }) => {
            state.isLoading = false
            state.user = payload
            state.error = null
        });

        builder.addCase(signUpAction.rejected, (state, { error }) => {
            state.isLoading = false
            state.error = error
        })
    }
});

export const { resetAuthSignUp } = signUpSlice.actions

export default signUpSlice.reducer