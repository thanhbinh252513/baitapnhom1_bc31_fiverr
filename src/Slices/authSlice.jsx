import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { message } from 'antd';
import { authAPI } from '../Service/authAPI';
import localService from '../Service/LocalService';

const initialState = {
    user: localService.user.get() || null,
    isLoading: false,
    error: null,
}

export const signInAction = createAsyncThunk(
    "auth/signin",
    async (values) => {
        try {
            const data = await authAPI.signInAPI(values)
            message.success("Sign In Success")
            localService.user.set(data)

            return data
        }
        catch (error) {
            message.error("Sign In Failed")
            throw error
        }
    }
)

const authSlice = createSlice({
    name: "auth",
    initialState,
    reducers: {
        resetAuth: (state) => {
            state.user = null;
            state.isLoading = false;
            state.error = null;
        }
    },

    extraReducers: (builder) => {
        builder.addCase(signInAction.pending, (state) => {
            state.isLoading = true
        });

        builder.addCase(signInAction.fulfilled, (state, { payload }) => {
            state.isLoading = false
            state.user = payload
        });

        builder.addCase(signInAction.rejected, (state, { error }) => {
            state.isLoading = false
            state.error = error
        })
    }
});

export const { resetAuth } = authSlice.actions

export default authSlice.reducer