import React from "react";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink, useNavigate } from "react-router-dom";
import SimpleSlider from "../../Slices/SlideBar/SlideBar";
// import { resetAuth } from "../../Slices/auth";

const HeaderHome = () => {
  //   const handleSubmit = (e) => {
  //     e.preventDefault();
  //   };
  /*eslint-disable*/
  const logo = (
    <svg
      width="300"
      height="18"
      viewBox="0 0 89 27"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      {" "}
      <g fill="#fff">
        <path d="m81.6 13.1h-3.1c-2 0-3.1 1.5-3.1 4.1v9.3h-6v-13.4h-2.5c-2 0-3.1 1.5-3.1 4.1v9.3h-6v-18.4h6v2.8c1-2.2 2.3-2.8 4.3-2.8h7.3v2.8c1-2.2 2.3-2.8 4.3-2.8h2zm-25.2 5.6h-12.4c.3 2.1 1.6 3.2 3.7 3.2 1.6 0 2.7-.7 3.1-1.8l5.3 1.5c-1.3 3.2-4.5 5.1-8.4 5.1-6.5 0-9.5-5.1-9.5-9.5 0-4.3 2.6-9.4 9.1-9.4 6.9 0 9.2 5.2 9.2 9.1 0 .9 0 1.4-.1 1.8zm-5.7-3.5c-.1-1.6-1.3-3-3.3-3-1.9 0-3 .8-3.4 3zm-22.9 11.3h5.2l6.6-18.3h-6l-3.2 10.7-3.2-10.8h-6zm-24.4 0h5.9v-13.4h5.7v13.4h5.9v-18.4h-11.6v-1.1c0-1.2.9-2 2.2-2h3.5v-5h-4.4c-4.3 0-7.2 2.7-7.2 6.6v1.5h-3.4v5h3.4z"></path>
      </g>
      <g fill="#1dbf73">
        <path d="m85.3 27c2 0 3.7-1.7 3.7-3.7s-1.7-3.7-3.7-3.7-3.7 1.7-3.7 3.7 1.7 3.7 3.7 3.7z"></path>
      </g>
    </svg>
  );
  const [render, setRender] = useState(true);
  const dispatch = useDispatch;
  const { user } = useSelector((state) => state.authSlice);
  const navigate = useNavigate();
  const handleClick = () => {
    navigate(`account-info/${user.taiKhoan}`);
  };

  const renderStatus = () => {
    if (user) {
      return (
        <>
          <div
            className=" hover:cursor-pointer"
            onClick={() => {
              navigate("/cart");
            }}
          >
            <i className="fa fa-shopping-cart text-white lg:text-2xl s:text-xs sm:text-base md:text-xl hover:cursor-pointer"></i>
          </div>
          <div
            onClick={() => handleClick()}
            className="overflow-hidden relative w-10 h-10 bg-gray-100 rounded-full dark:bg-gray-600  hover:cursor-pointer"
          >
            <img
              src="https://fiverr-res.cloudinary.com/image/upload/q_auto,f_auto/v1/attachments/generic_asset/asset/bb5958e41c91bb37f4afe2a318b71599-1599344049970/bg-hero-5-1792-x1.png"
              alt="avatar"
            ></img>
          </div>
          <a href="/">
            <button
              className="block text-white bg-[#383838] hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
              type="button"
              onClick={() => {
                setRender(false);
                localStorage.clear();
                dispatch(resetAuth());
              }}
            >
              Sign Out
            </button>
          </a>
        </>
      );
    } else {
      return (
        <div className="space-x-5">
          <NavLink
            to="/SignIn"
            className="lg:text-lg s:text-sm sm:text-base md:text-xl hover:bg-[#19A463] rounded-xl py-2 px-3 transition-all duration-500	"
          >
            Sign In
          </NavLink>
          <NavLink
            to="/SignUp"
            className="lg:text-lg s:text-sm sm:text-base md:text-xl hover:bg-[#19A463] rounded-xl py-2 px-3 transition-all duration-500	"
          >
            Sign Up
          </NavLink>
        </div>
      );
    }
  };

  return (
    <div className="bg-[#063B1B] h-20 text-white fixed top-0 left-0 z-50 w-full lg:text-lg md:text-md sm:text-base ">
      <div className="px-5 pt-2 flex  items-center space-x-5">
        <div className="">
          <NavLink className="logo relative" to="/">
            <div>{logo}</div>
          </NavLink>
        </div>

        <div className="w-2/5 rounded-2xl">
          <form
            onSubmit={(e) => {
              e.preventDefault();
            }}
          >
            <label
              htmlFor="default-search"
              className="mb-2 text-sm font-medium text-gray-900 sr-only dark:text-gray-300"
            >
              Search
            </label>

            <div className="relative">
              <div className="flex absolute inset-y-0 left-0 items-center pl-3 pointer-events-none">
                <svg
                  aria-hidden="true"
                  className="w-7 h-7 text-white"
                  fill="none"
                  stroke="currentColor"
                  viewBox="0 0 24 24"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={2}
                    d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
                  />
                </svg>
              </div>

              <input
                type="search"
                id="default-search"
                className="block p-4 pl-10 w-full text-xl font-medium text-white bg-[#383838] rounded-2xl 
                border
                focus:ring-blue-500 
                bg-transparent 
                border-gray-400 
                placeholder-white-400
                focus:ring-white-500 
                focus:border-white-500"
                placeholder="What service are you looking for today?"
                required
              />
            </div>
          </form>
        </div>

        <div className="text-base">
          <div className="flex items-center">
            <div className="flex space-x-5 mr-10">
              <button>
                <a href="" className="hover:text-[#19A463]">
                  Fiverr Business
                </a>
              </button>
              <button>
                <a href="" className="hover:text-[#19A463]">
                  Explore
                </a>
              </button>
              <button>
                <a href="" className="hover:text-[#19A463]">
                  English
                </a>
              </button>
              <button>
                <a href="" className="hover:text-[#19A463]">
                  $ USD
                </a>
              </button>
              <button>
                <a href="" className="hover:text-[#19A463]">
                  Become a Seller
                </a>
              </button>
            </div>
            {renderStatus()}
          </div>
        </div>
      </div>
    </div>
  );
};
export default HeaderHome;
