import React from "react";
import HeaderHome from "../../Components/HeaderHome/HeaderHome";
import SimpleSlider from "./../../Slices/SlideBar/SlideBar";

export default function HomePage() {
  return (
    <div>
      <HeaderHome />
      <div className="gabrielle">
        <div>
          <SimpleSlider />
        </div>
      </div>
    </div>
  );
}
