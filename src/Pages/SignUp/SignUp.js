import React, { useEffect, useState } from 'react';
import { Input, Tooltip, } from 'antd';
import { InfoCircleOutlined, UserOutlined, LockOutlined, EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons';
import { useDispatch } from 'react-redux';
import { signUpAction } from '../../Slices/signUpSlice';
import ImageAnimation from '../SignIn/ImageAnimation';


const logo = <svg width="600" height="27" viewBox="0 0 89 27" fill="none" xmlns="http://www.w3.org/2000/svg"> <g fill="#fff"><path d="m81.6 13.1h-3.1c-2 0-3.1 1.5-3.1 4.1v9.3h-6v-13.4h-2.5c-2 0-3.1 1.5-3.1 4.1v9.3h-6v-18.4h6v2.8c1-2.2 2.3-2.8 4.3-2.8h7.3v2.8c1-2.2 2.3-2.8 4.3-2.8h2zm-25.2 5.6h-12.4c.3 2.1 1.6 3.2 3.7 3.2 1.6 0 2.7-.7 3.1-1.8l5.3 1.5c-1.3 3.2-4.5 5.1-8.4 5.1-6.5 0-9.5-5.1-9.5-9.5 0-4.3 2.6-9.4 9.1-9.4 6.9 0 9.2 5.2 9.2 9.1 0 .9 0 1.4-.1 1.8zm-5.7-3.5c-.1-1.6-1.3-3-3.3-3-1.9 0-3 .8-3.4 3zm-22.9 11.3h5.2l6.6-18.3h-6l-3.2 10.7-3.2-10.8h-6zm-24.4 0h5.9v-13.4h5.7v13.4h5.9v-18.4h-11.6v-1.1c0-1.2.9-2 2.2-2h3.5v-5h-4.4c-4.3 0-7.2 2.7-7.2 6.6v1.5h-3.4v5h3.4z"></path></g><g fill="#1dbf73"><path d="m85.3 27c2 0 3.7-1.7 3.7-3.7s-1.7-3.7-3.7-3.7-3.7 1.7-3.7 3.7 1.7 3.7 3.7 3.7z"></path></g>
</svg>

export default function SignUp() {
    const dispatch = useDispatch()

    return (
        <div>
            <div className='bg-gray-900 h-screen w-screen'>
                <div className='container mx-auto flex space-x-10 justify-center items-center h-screen p-5'>
                    <div className='w-full h-full'>
                        <ImageAnimation />
                    </div>

                    <div className='w-full h-full text-3xl text-center'>
                        <div className='flex flex-col items-center h-full pt-5'>
                            <div className='flex-none h-1/4 w-full mt-5'>
                                {logo}
                            </div>

                            <div className='grow h-3/4 w-full'>
                                <h1 className='text-white'>Đăng Ký</h1>
                                <form action="">
                                    <Input
                                        size='large'
                                        placeholder="Enter your Email..."
                                        prefix={<UserOutlined className="site-form-item-icon" />}
                                        suffix={
                                            <Tooltip title="Extra information">
                                                <InfoCircleOutlined
                                                    style={{
                                                        color: 'rgba(0,0,0,.45)',
                                                    }}
                                                />
                                            </Tooltip>
                                        }
                                    />
                                    <br />
                                    <br />
                                    <Input.Password
                                        size='large'
                                        placeholder="Enter your Password... "
                                        prefix={<LockOutlined className="site-form-item-icon" />}
                                        iconRender={(visible) => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
                                    />
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div >
        </div>
    )
}
