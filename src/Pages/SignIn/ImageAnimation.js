import React from "react";
import Lottie from "lottie-react";
import bgAnimate from "../../assets/122259-working-woman.json";

export default function ImageAnimation() {
    return (
        <div className="">
            <Lottie animationData={bgAnimate} />
        </div>
    );
}

