const USER = "USER"

const localService = {
    user: {
        set: (dataUser) => {
            let dataJson = JSON.stringify(dataUser)
            localStorage.setItem(USER, dataJson)
        },
        get: () => {
            let dataJson = localStorage.getItem(USER)

            let checkDataJson = dataJson !== null
                ? (JSON.parse(dataJson))
                : (null)

            return checkDataJson
        },
        remove: () => {
            localStorage.removeItem(USER)
        }
    }
}

export default localService;