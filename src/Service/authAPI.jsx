import { axiosClient } from "./axiosClient"

export const authAPI = {
    signInAPI: (dataSignIn) => {
        return axiosClient.post("auth/signin", dataSignIn)
    },

    signUpAPI: (dataSignUp) => {
        return axiosClient.post("auth/signup", dataSignUp)
    },
}

