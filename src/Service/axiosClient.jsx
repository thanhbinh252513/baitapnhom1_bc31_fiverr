import axios from "axios";


const TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCAzMSIsIkhldEhhblN0cmluZyI6IjE5LzAyLzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY3Njc2NDgwMDAwMCIsIm5iZiI6MTY0ODQwMDQwMCwiZXhwIjoxNjc2OTEyNDAwfQ.2Pn1sQiOcYDhAQ2DqfnG78MdznvbWOk0pOmrJLVW9hs"

export const axiosClient = axios.create({
  baseURL: "https://fiverrnew.cybersoft.edu.vn/api/",
  header: {
    TokenCybersoft: TOKEN,
  },
});

